var express = require('express');
var router = express.Router();

const request=require('request')
const csv=require('csvtojson');

var csvController = require("./../controllers/csvController");

/* GET users listing. */
router.get('/', function(req, res, next) {
  csv({ noheader: true })
    .fromStream(request.get("https://firebasestorage.googleapis.com/v0/b/mytest-99cac.appspot.com/o/test.csv?alt=media&token=d098770e-6552-4be9-9507-cfc1282a124b"))
    // .fromStream(request.get("https://firebasestorage.googleapis.com/v0/b/mytest-99cac.appspot.com/o/All%20Staff%20Gmail.csv?alt=media&token=931c3b96-cbf8-4381-bc59-b613ecda4224"))
    .on('csv', async (csvRow) => {
      let name = `${csvRow[0]} ${csvRow[1]}`;
      let email = csvRow[2];
      let password = "123456";

      let { uid } = await csvController.addToAuth(email, password);
      
      let schoolId = "1002";
      let userInfo = {
        email: email,
        isadmin: false,
        iseditable: true,
        isremovable: true,
        isroot: false,
        name: name,
        password: password
      };
      let user = await csvController.addUserToDB(schoolId, userInfo, uid);
    })
    .on('done',(error)=>{
      //parsing finished
      console.log("----------------------- FINISHED -----------------------");
    })
    
  res.send('respond with a resource');
});

module.exports = router;
