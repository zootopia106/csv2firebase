var admin = require("firebase-admin");

var serviceAccount = require("../account.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://housepointsystem.firebaseio.com"
});

let auth = admin.auth();
let db = admin.database().ref("users");

module.exports.addToAuth = (email, password) => {
  return admin.auth().createUser({
    email: email,
    password: password
  });
}

module.exports.addUserToDB = (schoolId, userInfo, uid ) => {
  return db.child(`${schoolId}/${uid}`).set(userInfo);
}

